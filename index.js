const express = require("express");
const app = express();
const pool = require("./db");
const {body, validationResult} = require("express-validator");

app.use(express.json());

// GET ALL
app.get("/contact-us", async (req, res) => {
    try {
        const getAllContactUs = await pool.query("SELECT * FROM contact_us");

        res.status(200).json(
            {message: "get success",
            data: getAllContactUs.rows}
        );
    } catch (error) {
        console.error(error.message);
    }
})

// GET ONE
app.get("/contact-us/:id", async (req, res) => {
    try {
        const {id} = req.params;
        const getContactUs = await pool.query("SELECT * FROM contact_us WHERE id = $1",[id]);

        res.status(200).json(
            {message: "get success",
            data: getContactUs.rows}
        );
    } catch (error) {
        console.error(error.message);
    }
})

// POST
app.post("/contact-us", body('email').isEmail(), async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
        }

        const { email, message } = req.body;
        const newContactUs = await pool.query("INSERT INTO contact_us (email, message) VALUES ($1, $2) RETURNING *",[email, message]);

        res.status(200).json(
            {message: "post success",
            data: newContactUs.rows}
        );
    } catch (err) {
        console.error(err.message)
    }
})

// PUT
app.put("/contact-us/:id", body('email').isEmail(), async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
        }

        const { email, message } = req.body;
        const { id } = req.params;
        const updateContactUs = await pool.query("UPDATE contact_us SET email = $1, message = $2 WHERE id = $3 RETURNING *",[email, message, id]);

        res.status(200).json(
            {message: "get success",
            data: updateContactUs.rows}
        );
    } catch (err) {
        console.error(err.message)
    }
})

// DELETE
app.delete("/contact-us/:id", async (req, res) => {
    try {
        const { id } = req.params;
        const deleteContactUs = await pool.query("DELETE FROM contact_us WHERE id = $1",[id]);

        res.status(200).json(
            {message: "delete success"}
        );
    } catch (err) {
        console.error(err.message)
    }
})

app.listen(3000, () => {
    console.log("server is listening on port 3000");
})